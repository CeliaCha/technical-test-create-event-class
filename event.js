const { Utils } = require("./utils")

/** @type {number} */
const SLOT_DURATION = 1800000 // minimum time slot duration in milliseconds (30 minutes here)

/** @type {Object.<string, string>} */
const ERRORS = {
  datesOrder : 'Start date should be anterior to end date',
  recurringEventDates : 'Start date and end date of a recurring event should be on the same day'
}

/**
 * Event object constructor
 * @param {boolean} opening
 * @param {boolean} recurring
 * @param {Date} startDate
 * @param {Date} endDate
 */
const Event = function (opening, recurring, startDate, endDate) {
  // Handle app limitations :
  if (startDate >= endDate) {
    throw new Error(ERRORS.datesOrder)
  } else if (recurring && startDate.toLocaleDateString() !== endDate.toLocaleDateString()) {
    throw new Error(ERRORS.recurringEventDates)
  }

  this.opening = opening;
  this.recurring = recurring;
  this.startDate = startDate;
  this.endDate = endDate;

  this.eventList.push(this);
}

/** @type {array<EventObject>} */
Event.prototype.eventList = []

/** @type {Object.<string, array<Date>>} */
Event.prototype.availabilitiesByDay = {}

/**
 * Event method : get availabilities in the input period
 * @param {Date} startDate
 * @param {Date} endDate
 * @return {Array<Date>}
 */
Event.prototype.availabilities = function(fromDate, toDate){
  if (fromDate >= toDate) {
    throw new Error(ERRORS.datesOrder)
  } 

  // Generate unique events from recurring events whose weekday is included in the input period :
  this.generateUniqueEventsFromRecurring(fromDate, toDate)

  // Get unique events whose date range matches with the input date range : 
  const uniqueEvents = this.eventList.filter(event => event.recurring === false)
  const matchingEvents = this.getMatchingEvents(fromDate, toDate, uniqueEvents)

  // Map matching events to a dictionary object of availabilities by date :
  this.mapEventsToAvailabilities(fromDate, toDate, matchingEvents)

  // Transform dictionary object to flat array :
  const availabilitiesDates = this.getAvailabilitiesList()

  // Return something as awesome as I can do :
  return availabilitiesDates
}

/**
 * Generate console messages to display availabilities in user langage
 */
Event.prototype.logAvailabilitiesMessage = function () {
  const availableDays = Object.keys(this.availabilitiesByDay)

  if (availableDays.length === 0) {
    console.log("Sorry, I have no availabilities in this period.")
    return
  }

  for (const day of availableDays) {
    const formattedDay = new Date(day).toDateString()
    const messageStart = `I'm available on ${formattedDay}, at `

    const availableHours = this.availabilitiesByDay[day].reduce((acc, time, index) => {
      const datetime = time.toLocaleTimeString('fr-FR', {hour: '2-digit', minute:'2-digit'})
      if (index === this.availabilitiesByDay[day].length - 1 && this.availabilitiesByDay[day].length > 1) {
        acc = acc + `and ${datetime}.`
      } else {
        const slotEnd = this.availabilitiesByDay[day].length > 1 ? ', ' : '.'
        acc = acc + `${datetime}${slotEnd}`
      }
      return acc
    }, messageStart)

    console.log(availableHours);
  }
  console.log("I'm not available any other time !")
}

/**
 * Generate unique events from recurring events whose weekday is included in the input daterange
 * @param {Date} fromDate
 * @param {Date} toDate
 */
Event.prototype.generateUniqueEventsFromRecurring = function (fromDate, toDate) {
  const recurringEvents = this.eventList.filter(event => event.recurring === true)
  const datesIncludedInDateRange = Utils.getDatesInDateRange(fromDate, toDate)

  // Add to eventList unique events corresponding to recurring events matching with the input period :
  for (const date of datesIncludedInDateRange) {
    for (const {opening, startDate, endDate} of recurringEvents) {
      if (startDate.getDay() === date.getDay() ) {
        // Computed startDate :
        const newStartDate = new Date(date)
        newStartDate.setHours(startDate.getHours(), startDate.getMinutes())

        // Computed endDate :
        const newEndDate = new Date(date) // we assume that startDate and endDate of recurring events are defined on a "one day" basis
        newEndDate.setHours(endDate.getHours(), endDate.getMinutes())

        new Event(opening, false, newStartDate, newEndDate)
      }
    }
  }
}

/**
 * Flatten dictionary of dates to date list
 * @returns {Array<Date>}
 */
Event.prototype.getAvailabilitiesList = function () {
  const days = Object.keys(this.availabilitiesByDay)
  return days.reduce((acc, day) => {
    acc.push(...this.availabilitiesByDay[day])
    return acc
  }, [])
}

/**
 * Get events whose date range overlap the input daterange
 * @param {Date} fromDate
 * @param {Date} toDate
 * @param {array<EventObject>} eventsArray
 * @returns {array<EventObject>}
 */
Event.prototype.getMatchingEvents = function (fromDate, toDate, eventsArray) { 
  return eventsArray.filter(event => event.startDate <= toDate && event.endDate >= fromDate)
}


/**
 * Generate a dictionary object with list of availabilities for each date
 * @param {Date} fromDate
 * @param {Date} toDate
 * @param {array<EventObject>} events
 */
Event.prototype.mapEventsToAvailabilities = function (fromDate, toDate, events) {
  const openingEvents = events.filter(event => event.opening === true)
  const busyEvents = events.filter(event => event.opening === false)

  // Generate dictionary object :
  /** @type {Object.<string, array<Date>>}**/
  const availabilitiesByDay = openingEvents.reduce((acc, {startDate, endDate}) => {
    let time = startDate.getTime()
    const endTime = endDate.getTime() - SLOT_DURATION // interval smaller than time slot duration should not be counted as available for reservation
    const availabilityList = []

    // Add a datetime to list for each availability, based on SLOT_DURATION :
    while (time <= endTime) {

      // Check limits of input period before adding availability :
      if (time >= fromDate.getTime() && time + SLOT_DURATION <= toDate.getTime()) {

        // Check if datetime if within a busy time period, or before a busy time period when available time for an appointment is inferior to SLOT_DURATION :
        let isBusyTime = false
        for (const busyEvent of busyEvents) {
          if ((time >= busyEvent.startDate || busyEvent.startDate.getTime() - time < SLOT_DURATION) && time < busyEvent.endDate) {
            isBusyTime = true
          }
        }

        if (!isBusyTime) {availabilityList.push(new Date(time)) }
      }
      time += SLOT_DURATION // increment slot duration to get next availability time
    }

    // Generate key for each day with some availabilities :
    if (availabilityList.length > 0) {
      const day = startDate.toDateString()
      acc[day] = availabilityList
    }

    return acc
  }, ({}))

  this.availabilitiesByDay = availabilitiesByDay
}

/**
 * Clear events data
 */
Event.prototype.resetEventList = function() { // for testing purpose
  this.eventList = []
}

/**
 * @typedef {Object} EventObject // 'Event' is already declared as a global object in Node context, it's why we choose the name EventObject
 * @property {boolean} opening
 * @property {boolean} recurring
 * @property {Date} startDate
 * @property {Date} endDate
 */

module.exports = { Event }
