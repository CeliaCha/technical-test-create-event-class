const chai = require('chai')
chai.use(require('chai-datetime'))
const expect = require('chai').expect

const { Event } = require('../event')

process.env.TZ = 'UTC' // fix timezone problems

const SLOT_DURATION = 1800000

describe('event.js tests', () => {
    describe('Event constructor Test', () => {
        it('should create a valid Event object', () => {
            const startDate = new Date(2022,6,1,10,30) // July 1st, 10:30
            const endDate = new Date(2022,6,1,14,00) // July 1st, 14:00
            const result = new Event(true, true, startDate, endDate)
            expect(result).to.be.instanceOf(Event)

            Event.prototype.resetEventList()
        })
    })

    describe('Recurring events test', () => {
        it('should return events inside input period matching with recurring events', () => {
            const recurringStartDate = new Date(2016,6,1,10,30) // 2016 July 1st, 10:30 > recurring on Wednesdays
            const recurringEndDate = new Date(2016,6,1,14,00) // 2016 July 1st, 14:00 > recurring on Wednesdays
            new Event(true, true, recurringStartDate, recurringEndDate) // weekly recurring opening in calendar
            
            const fromDate = new Date(2022,6,6,12,00) // 2022 July 6th, 12:00
            const toDate = new Date(2022,6,27,11,00) // 2022 July 27th, 11:00

            Event.prototype.generateUniqueEventsFromRecurring(fromDate, toDate)

            const uniqueEvents = Event.prototype.eventList.filter(event => event.recurring === false)
            const result = Event.prototype.getMatchingEvents(fromDate, toDate, uniqueEvents)
            expect(result).to.have.lengthOf(3)

            Event.prototype.resetEventList()
        })
    })
    
    describe('Busy events test', () => {
        it('should not create availabilities slots inside busy periods', () => {
            const recurringStartDate = new Date(2015,6,1,10,30)  // 2015 July 1st, 10:30 > recurring on Wednesdays
            const recurringEndDate = new Date(2015,6,1,14,00)  // 2015 July 1st, 10:30 > recurring on Wednesdays
            new Event(true, true, recurringStartDate, recurringEndDate) // weekly recurring opening in calendar
            
            const busyStartDate = new Date(2016,6,20,11,30) // 2016 July 20th 11:30
            const busyEndDate = new Date(2016,6,20,12,30) // 2016 July 20th 12:30
            new Event(false, false, busyStartDate, busyEndDate) // intervention scheduled
            
            const fromDate = new Date(2016,6,6,12,00)  // 2016 July 6st 12:00
            const toDate = new Date(2016,6,27,11,00) // 2016 July 27st 11:00
            
            const result = Event.prototype.availabilities(fromDate, toDate)
            for (const date of result) {
                expect(date.getTime()).to.not.be.within(busyStartDate.getTime() + 1 , busyEndDate.getTime() - 1)
            }

            Event.prototype.resetEventList()
        })
    })

    describe('Input period limits tests', () => {
        it('should not create availabilities slots outside input period', () => {
            const recurringStartDate = new Date(2015,6,1,10,30) // July 1st, 10:30
            const recurringEndDate = new Date(2015,6,1,14,00) // July 1st, 14:00
            new Event(true, true, recurringStartDate, recurringEndDate) // weekly recurring opening in calendar
            
            const fromDate = new Date(2016,6,6,12,00)  // 2016 July 6st 12:00
            const toDate = new Date(2016,6,27,11,00) // 2016 July 27st 11:00

            const result = Event.prototype.availabilities(fromDate, toDate)
            for (const date of result) {
                expect(date).to.not.be.beforeDate(fromDate)
                expect(date).to.not.be.afterDate(toDate)
            }

            Event.prototype.resetEventList()
        })
        it('should not propose a timeslot when available time is smaller than SLOT_DURATION', () => {
            const recurringStartDate = new Date(2015,6,1,10,30) // July 1st, 10:30
            const recurringEndDate = new Date(2015,6,1,14,00) // July 1st, 14:00
            new Event(true, true, recurringStartDate, recurringEndDate) // weekly recurring opening in calendar
            
            const fromDate = new Date(2016,6,6,12,00)  // 2016 July 6st 12:00
            const toDate = new Date(2016,6,27,11,00) // 2016 July 27st 11:00

            const result = Event.prototype.availabilities(fromDate, toDate)
            for (const date of result) {
                expect(date.getTime() + SLOT_DURATION).to.be.below(toDate.getTime() + 1)
            }

            Event.prototype.resetEventList()
        })
    })
})