const { Event } = require("./event")

process.env.TZ = 'UTC' // fix timezone problems

const recurringStartDate = new Date(2015,6,1,10,30) // 2015 July 1st, 10:30
const recurringEndDate = new Date(2015,6,1,14,00) // 2015 July 1st, 14:00
new Event(true, true, recurringStartDate, recurringEndDate) // weekly recurring opening in calendar

const busyStartDate = new Date(2022,6,20,11,30) // 2022 July 20th 11:30
const busyEndDate = new Date(2022,6,20,12,30) // 2022 July 20th 12:30
new Event(false, false, busyStartDate, busyEndDate) // intervention scheduled

const fromDate = new Date(2022,6,6,12,00) // 2022 July 6th 12:00
const toDate = new Date(2022,6,27,11,00) // 2022 July 27th 11:00

const result = Event.prototype.availabilities(fromDate, toDate)

// Display data in console :
console.log([
  {recurringStartDate : recurringStartDate.toLocaleString()},
  {recurringEndDate: recurringEndDate.toLocaleString()},
  {busyStartDate: busyStartDate.toLocaleString()},
  {busyEndDate: busyEndDate.toLocaleString()},
  {fromDate : fromDate.toLocaleString()}, 
  {toDate: toDate.toLocaleString()}
])
console.table(result.map(date => date.toLocaleString()))
Event.prototype.logAvailabilitiesMessage()

/*
 * Answer should be : 
 * I'm available from July 8th, at 10:30, 11:00, 12:30, 13:00, and 13:30
 * I'm not available any other time !
 */