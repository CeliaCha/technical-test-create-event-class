const Utils = {
  /**
   * Get all dates in a specified period
   * @param {Date} fromDate 
   * @param {Date} toDate
   * @returns {array<Date>}
   */
  getDatesInDateRange : function (fromDate, toDate) {
    const date = this.getNoon(fromDate);
    const end = this.getNoon(toDate)
    const dates = [];
    while (date <= end) {
      dates.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
    return dates;
  },

  /**
   * Get noon time for a date
   * @param {Date} date 
   * @returns {Date}
   */
  getNoon : function (date) {
    const noon = new Date(date)
    noon.setHours(12, 0, 0)
    return noon
  }
}

module.exports = { Utils }